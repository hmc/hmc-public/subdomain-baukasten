# Subdomain Baukasten

This repository contains information and specification of minimal and nice to have requirements to create a HMC-subdomain and how they can be easily implemented.

The requirements and wishes are specified by the HMC office in the wiki of this repository, as well as legal requirements for privacy and impress statements required for each sub domain.

Creators and maintainers of subdomains can exchange tools here.

Important elements (e.g. logos, fonts) will be centrally updated. Currenty, this Repository contains:
- logos in .png and .svg format
- fonts (please note, that these fonts are only allowed to be used in HMC Subdomains due to license reasons)
- styleguides to look at when you are not sure how to implement special specifications

The Wiki of this repository holds information about:
- general overview of subdomain layouts and mockups
- colour uses
- use and look alike of UI elements
- information about images 
- requirments of data privacy (coming soon)
- requiremets of imprint (coming soon)

Get started with the Wiki [here](https://codebase.helmholtz.cloud/hmc/hmc-public/subdomain-baukasten/-/wikis/Overview:-Subdomain-requirements).

If you miss something, don't hesitate to contact vivien.serve@helmholtz-berlin.de